# Ionic Selectable Demo
A demo app for [Ionic Selectable](https://github.com/eakoriakin/ionic-selectable) component. Check out [live demo](https://stackblitz.com/edit/ionic-selectable-basic?file=pages%2Fhome%2Fhome.html).

![iOS Demo](https://raw.githubusercontent.com/eakoriakin/ionic-selectable/master/demo/ios.gif)

## Getting started
Just install dependencies and run the app.
```
npm install
ionic serve
```
